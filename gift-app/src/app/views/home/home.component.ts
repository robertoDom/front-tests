import { Component } from '@angular/core';
import { HttpService } from '../../services/http/http.service';
import { Observable, Subscription } from 'rxjs';
import { MediaService } from '../../services/media/media.service';
import { CommonModule } from '@angular/common';
import { MediaCardComponent } from '../../components/media-card/media-card.component';
import { SearchBarComponent } from '../../components/search-bar/search-bar.component';
import { CategoriesService } from '../../services/categories/categories.service';
import { ChipComponent } from '../../components/chip/chip.component';
import { IMedia } from '../../models/Media.model';
import { FirebaseService } from '../../services/firebase/firebase.service';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, MediaCardComponent, SearchBarComponent, ChipComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {

  public mediaList$ = this.mediaService.media$;
  public categoryList$ = this.categoriesService.category$;
  public searchList : string[] = [];

  private firebaseSubscription = new Subscription();

  constructor(
    private httpService: HttpService,
    private mediaService: MediaService,
    private categoriesService: CategoriesService,
    private firebaseService: FirebaseService
  ) { }

  async ngOnInit() {
    await this.httpService.getTrendGifts();
    await this.firebaseService.getLastSearch();
    this.firebaseSubscription = this.firebaseService.getLastSearch().subscribe(data=>{
      this.searchList = data.map(search => search['search'] as string);
    });
  }

  async onSelectCategory(id: number, value: string) {
    this.categoriesService.setCategorySelected(id);
    if (id == 3) {
      await this.firebaseService.getFavorites();
      return;
    }
    await this.httpService.searchGifts(value);
  }

  async onSearchValue(value: string) {
    await this.httpService.searchGifts(value);
    await this.firebaseService.saveLastSearch(value);
  }

  async onLike(media: IMedia) {
    await this.firebaseService.saveFavorite(media);
  }

  async onDownload(media: IMedia){
    this.mediaService.downloadMedia(media.images.original.url,media.title);
  }
}
