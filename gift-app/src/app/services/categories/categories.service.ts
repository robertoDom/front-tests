import { Injectable } from '@angular/core';
import { ICategory } from '../../models/Category.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  // genero esta lista estatica debido a que las categorias en si son muy variadas y mediante el endpoint de search logro un mejor resultado
  private categoryList: ICategory[] = [
    { // hardcode para traer favoritos en una seccion 
      id: 3,
      label: 'Favoritos',
      value: 'N/A',
      selected: false
    },
    {
      id: 0,
      label: 'Perritos',
      value: 'perrito, perritos, dog, dogs',
      selected: false
    },
    {
      id: 1,
      label: 'Gatitos',
      value: 'gatitos, gatito, cat, cats',
      selected: false
    },
  ];

  private categorySubject: BehaviorSubject<ICategory[]> = new BehaviorSubject<ICategory[]>(this.categoryList);
  public category$: Observable<ICategory[]> = this.categorySubject.asObservable();

  constructor() { }

  public setCategorySelected(id: number) {
    this.categoryList.forEach(category => category.selected = false);
    let categoryIndex = this.categoryList.findIndex(category => category.id == id);

    if (categoryIndex != -1) {
      this.categoryList[categoryIndex].selected = true;
    }

    this.categorySubject.next([...this.categoryList]);
  }
}
