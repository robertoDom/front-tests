import { Injectable, inject } from '@angular/core';
import { Firestore, addDoc, collection, collectionData, deleteDoc, limitToLast, orderBy, query, where } from '@angular/fire/firestore';
import { IMedia } from '../../models/Media.model';
import { lastValueFrom } from 'rxjs';
import { MediaService } from '../media/media.service';
@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  private firestore: Firestore = inject(Firestore);

  constructor(
    private mediaService: MediaService
  ) { }

  async initIndexDB() {
    let user = localStorage.getItem("user");
    if (!user) {
      const userProfileCollection = collection(this.firestore, 'users');

      let document = await addDoc(userProfileCollection, { created_at: new Date() });
      localStorage.setItem("user", document.id);
      return;
    }

  }

  async saveFavorite(media: IMedia) {
    return new Promise((resolve, reject) => {
      let user = localStorage.getItem("user");

      const userProfileCollection = collection(this.firestore, `users/${user}/favorites`);
      let favorite = collectionData(query(userProfileCollection, where('media.id', '==', media.id)));
      favorite.subscribe(async (data) => {
        if (data.length <= 0) {
          let document = await addDoc(userProfileCollection, { media });
          resolve(true);
        }
      })
      return true;
    })
  }

  async getFavorites() {
    let mediaList = await new Promise<IMedia[]>((resolve, reject) => {
      let user = localStorage.getItem("user");

      const userProfileCollection = collection(this.firestore, `users/${user}/favorites`);
      let favorite = collectionData(query(userProfileCollection));
      favorite.subscribe(async (data) => {
        let mediaList = data.map((data: any) => {
          let media = data.media as IMedia;
          media.selected = true;
          return media;
        });
        
        resolve(mediaList as IMedia[])
      })
    })

    this.mediaService.setMediaList(mediaList);
  }

  async getReferenceFavorites() {
    let mediaList = await new Promise<IMedia[]>((resolve, reject) => {
      let user = localStorage.getItem("user");

      const userProfileCollection = collection(this.firestore, `users/${user}/favorites`);
      let favorite = collectionData(query(userProfileCollection));
      favorite.subscribe(async (data) => {
        let mediaList = data.map((data: any) => data.media);
        resolve(mediaList as IMedia[])
      })
    })

    return mediaList;
  }


  async saveLastSearch(search: string) {
    return new Promise(async (resolve, reject) => {
      let user = localStorage.getItem("user");

      const userProfileCollection = collection(this.firestore, `users/${user}/search`);
      await addDoc(userProfileCollection, { search: search, created_at: new Date() });
      resolve(true)
    });
  }

  // retorno un observable para siempre escuchar las busquedas mas recientes del firebase
  public getLastSearch() {
    let user = localStorage.getItem("user");

    const userProfileCollection = collection(this.firestore, `users/${user}/search`);
    let result = collectionData(query(userProfileCollection, orderBy('created_at'), limitToLast(5)));
    return result;
  }
}
