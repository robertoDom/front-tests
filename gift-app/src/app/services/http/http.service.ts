import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { lastValueFrom } from 'rxjs';
import { IGeneralResponse } from '../../models/responses/GeneralResponse.dto';
import { IMedia } from '../../models/Media.model';
import { MediaService } from '../media/media.service';
import { FirebaseService } from '../firebase/firebase.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private httpClient: HttpClient,
    private mediaService: MediaService,
    private firebaseService : FirebaseService
  ) { }

  public async getRandomGifts(): Promise<IGeneralResponse<IMedia>> {
    try {
      let url = `${environment.API_URL}/${environment.ENDPOINTS.RANDOM}?api_key=${environment.API_KEY}&tag=&rating=r`;
      let request = this.httpClient.get<IGeneralResponse<IMedia>>(url);
      let response = await lastValueFrom(request);
      return response;
    } catch (error) {
      throw false;
    }
  }

  public async getTrendGifts(): Promise<Boolean> {
    try {
      let url = `${environment.API_URL}/${environment.ENDPOINTS.TREND}?api_key=${environment.API_KEY}&limit=25&offset=0&rating=g&bundle=messaging_non_clips`;
      let request = this.httpClient.get<IGeneralResponse<IMedia>>(url);
      let response = await lastValueFrom(request);
      let likedMediaList = await this.firebaseService.getReferenceFavorites();
      let mediaList = response.data as IMedia[];

      likedMediaList.forEach(media =>{
        let mediaIndex = mediaList.findIndex(mediaRef => mediaRef.id == media.id);
        if(mediaIndex != -1){
          mediaList[mediaIndex].selected = true;
        }
      })
      
      this.mediaService.setMediaList(mediaList);

      return true;
    } catch (error) {
      throw false;
    }
  }

  public async searchGifts(value: string): Promise<Boolean> {
    try {
      let url = `${environment.API_URL}/${environment.ENDPOINTS.SEARCH}?api_key=${environment.API_KEY}&q=${value}&limit=50&offset=0&rating=g&lang=en&bundle=messaging_non_clips`;
      let request = this.httpClient.get<IGeneralResponse<IMedia>>(url);
      let response = await lastValueFrom(request);
      let likedMediaList = await this.firebaseService.getReferenceFavorites();
      let mediaList = response.data as IMedia[];

      likedMediaList.forEach(media =>{
        let mediaIndex = mediaList.findIndex(mediaRef => mediaRef.id == media.id);
        if(mediaIndex != -1){
          mediaList[mediaIndex].selected = true;
        }
      })
      this.mediaService.setMediaList(mediaList);

      return true;
    } catch (error) {
      throw false;
    }
  }

}
