import { Injectable } from '@angular/core';
import { IMedia } from '../../models/Media.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MediaService {
  private mediaList: IMedia[] = [];
  private mediaSubject: BehaviorSubject<IMedia[]> = new BehaviorSubject<IMedia[]>(this.mediaList);
  public media$: Observable<IMedia[]> = this.mediaSubject.asObservable();

  private historyList: IMedia[] = [];
  private historySubject: BehaviorSubject<IMedia[]> = new BehaviorSubject<IMedia[]>(this.historyList);
  public history$: Observable<IMedia[]> = this.mediaSubject.asObservable();

  constructor() { }

  public setMediaList(list: IMedia[]) {
    this.mediaSubject.next([...list]);
  }

  public setHistoryList(list: IMedia[]) {
    this.historySubject.next([...list]);
  }

  public async downloadMedia(url : string,name :string){
    let a = document.createElement('a');
    let response = await fetch(url);
    let file = await response.blob();
    a.download = name;
    a.href = window.URL.createObjectURL(file);
    a.dataset['downloadurl'] = ['application/octet-stream', a.download, a.href].join(':');
    a.click();
  }
}
