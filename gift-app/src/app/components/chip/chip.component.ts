import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-chip',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './chip.component.html',
  styleUrl: './chip.component.css'
})
export class ChipComponent {
  @Input() id: number = 0;
  @Input() label: string = "";
  @Input() value: string = "";
  @Input() selected: boolean = false;

  @Output() onSelectCategory = new EventEmitter<number>();

  public selectCategory(id: number) {
    this.onSelectCategory.emit(id)
  }
}
