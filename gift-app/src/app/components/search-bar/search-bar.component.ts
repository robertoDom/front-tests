import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './search-bar.component.html',
  styleUrl: './search-bar.component.css'
})
export class SearchBarComponent {
  @Input() searchList : string[] = [];
  @Output() onSearch = new EventEmitter();

  public searchValue: string = "";
  public showHistory : boolean = false;

  public search() {
    this.onSearch.emit(this.searchValue)
  }

  public searcHistory(searchValue : string) {
    this.searchValue = searchValue;
    this.onSearch.emit(searchValue);
  }

  public clear() {
    this.searchValue = '';
  }

  public onMouseLeave(){
    this.showHistory = false;
  }
}
