import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IMedia } from '../../models/Media.model';
import { MediaTypeEnum } from '../../models/Enums/MediaType.enum';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from '../loader/loader.component';

@Component({
  selector: 'app-media-card',
  standalone: true,
  imports: [CommonModule,LoaderComponent],
  templateUrl: './media-card.component.html',
  styleUrl: './media-card.component.css'
})
export class MediaCardComponent {
  @Input() url: string = "";
  @Input() id: string = "";
  @Input() name: string = "";
  @Input() liked: boolean = false;
  @Input() media!: IMedia;

  @Output() onLike = new EventEmitter<IMedia>();
  @Output() onDownload = new EventEmitter<boolean>();

  public showLoader : boolean = true;

  public setLike() {
    this.liked = !this.liked;
    this.onLike.emit(this.media);
  }

  public download(){
    this.onDownload.emit(true);
  }

  public onLoad(){
    this.showLoader = false;
  }
}
