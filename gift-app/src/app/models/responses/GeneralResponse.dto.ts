export interface IGeneralResponse<T> {
  data: T | T[],
  meta: {
    status: number;
    msg: string;
    response_id: string;
  }
}