export interface ICategory {
  id: number;
  label: string;
  value: string;
  selected: boolean;
}