import Vue from 'vue'
import App from './App.vue'

import ActionButtonComponent from "@/components/ActionButtonComponent.vue";
Vue.component('ActionButtonComponent', ActionButtonComponent);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
