export const StatusEnum = Object.freeze({
  SUCCESS: "success",
  DANGER: "danger",
  INFO: "info",
  INFO_OUTLINE: "info-outline"
});