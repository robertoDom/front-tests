

export default class HttpService {
  constructor() {
    this.URLS = {
      MAIN_API: "https://dummyjson.com",
      USER_ENDPOINT: "users",
      SEARCH_USER: "users/search"
    }
  }

  async getUsers(skip, limit) {
    try {
      let userUrl = `${this.URLS.MAIN_API}/${this.URLS.USER_ENDPOINT}?select=id,firstName,lastName,age,height,weight`;
      if (limit && skip) {
        userUrl += `&limit=${limit}&skip=${skip}`
      }

      let response = await this.httpRequest(userUrl, 'GET', null)
      return response.users;
    } catch (error) {
      console.log(error)
      throw false;
    }
  }

  async getUserById(userId) {
    try {
      let userUrl = `${this.URLS.MAIN_API}/${this.URLS.USER_ENDPOINT}/${userId}`;
      let response = await this.httpRequest(userUrl, 'GET', null)
      return response;
    } catch (error) {
      console.log(error)
      throw false;
    }
  }

  async createUser(user) {
    try {
      let userUrl = `${this.URLS.MAIN_API}/${this.URLS.USER_ENDPOINT}/add`;
      let response = await this.httpRequest(userUrl, 'POST', JSON.stringify(user))
      return response;
    } catch (error) {
      console.log(error)
      throw false;
    }
  }

  async updateUser(user, userId) {
    try {
      let userUrl = `${this.URLS.MAIN_API}/${this.URLS.USER_ENDPOINT}/${userId}`;
      let response = await this.httpRequest(userUrl, 'PUT', JSON.stringify(user))
      return response;
    } catch (error) {
      console.log(error)
      throw false;
    }
  }

  async deleteUser(userId) {
    try {
      let userUrl = `${this.URLS.MAIN_API}/${this.URLS.USER_ENDPOINT}/${userId}`;
      let response = await this.httpRequest(userUrl, 'DELETE', null)
      return response;
    } catch (error) {
      console.log(error)
      throw false;
    }
  }

  async searchUser(search) {
    try {
      let userUrl = `${this.URLS.MAIN_API}/${this.URLS.SEARCH_USER}?q=${search}`;
      let response = await this.httpRequest(userUrl, 'GET', null)
      return response.users;
    } catch (error) {
      console.log(error)
      throw false;
    }
  }

  async httpRequest(url, method, body) {
    try {
      let response = await fetch(url, {
        method: method,
        body: body
      });

      return response.json();
    } catch (error) {
      console.log(error)
      throw false;
    }
  }
}